const serverLocation = window.location.hostname

fetch(`http://${serverLocation}:8081/`).then(x => console.log(x))

const socket = new WebSocket(`ws://${serverLocation}:8081/wsapp/debugger`)

socket.addEventListener("open", _ => {
  socket.send("start")
})

let lastThreadRef = undefined

socket.addEventListener('message', event => {
  const message = JSON.parse(event.data)
  console.log(message)
  lastThreadRef = message.threadName
})

document.getElementById("step-button")
  .addEventListener('click', _ => {
    const stepRequest = {
      "$type": "felixdb.CommandMessage.Step",
      "threadName": lastThreadRef
    }
    console.log(stepRequest)
    socket.send(JSON.stringify(stepRequest))
  })
