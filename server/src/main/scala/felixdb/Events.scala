package felixdb

import com.sun.jdi._
import com.sun.jdi.event._
import com.sun.jdi.request._
import collection.JavaConverters._
import java.util.concurrent.LinkedBlockingQueue
import scala.collection.concurrent.TrieMap
import scala.concurrent.{Future, blocking}
import scala.concurrent.ExecutionContext.Implicits.global

final case class Breakpoint(lineNumber: Int)

class Events(
  vm: VirtualMachine,
  breakpoints: List[Breakpoint],
  messages: LinkedBlockingQueue[DebugMessage],
  commands: LinkedBlockingQueue[CommandMessage]
) {

  lazy val eventRequestManager = vm.eventRequestManager()
  lazy val threadReferences = new TrieMap[String, ThreadReference]()

  def enableClassPrepareEvents(): Unit = {
    val cpr = eventRequestManager.createClassPrepareRequest()
    cpr.addSourceNameFilter("*MainSpec.scala")
    cpr.enable()
  }

  def setBreakpoints(classPrepareEvent: ClassPrepareEvent): Unit =
    classPrepareEvent.referenceType() match {
      case classType: ClassType =>
        breakpoints.foreach { bp =>
          val loc = classType.locationsOfLine(bp.lineNumber).asScala.head
          eventRequestManager.createBreakpointRequest(loc).enable
        }
      case unknown =>
        throw new Exception(s"unknown type: ${unknown}")
    }

  def registerThread(thread: ThreadReference): Unit =
    threadReferences += (thread.name -> thread)

  def displayVaribles(locatable: LocatableEvent): Unit = {
    registerThread(locatable.thread)
    val stackFrame = locatable.thread.frame(0)

    val visibleValues = stackFrame.getValues(
      stackFrame.visibleVariables()
    ).asScala

    visibleValues.foreach { case (varible, value) =>
      messages.add(
        DebugMessage.Varible(
          locatable.thread.name,
          varible.name,
          varible.`type`.name,
          value.toString
        )
      )
    }
  }

  def listenForCommands(): Unit =
    Future {
      blocking {
        while (true) {
          commands.take() match {
            case CommandMessage.Step(threadName) =>
              enableStepRequest(
                threadReferences(threadName)
              )
          }
        }
      }
    }

  def enableStepRequest(thread: ThreadReference): Unit = {
    println(s"creating step request for ${thread.name}")
    val stepRequest = eventRequestManager.createStepRequest(
      thread, StepRequest.STEP_LINE, StepRequest.STEP_OVER
    )
    stepRequest.enable()
    vm.resume()
  }

  def eventSets(): Iterator[EventSet] =
    new Iterator[EventSet] {
      def next(): EventSet =
        vm.eventQueue().remove()
      def hasNext: Boolean =
        true
    }

  def start(): Unit = {
    listenForCommands()
    enableClassPrepareEvents()

    try {
      for {
        eventSet <- eventSets()
        event <- eventSet.eventIterator().asScala
      } {
        println(s"handling event ${event}")
        event match {

          case cpe: ClassPrepareEvent =>
            setBreakpoints(cpe)

          case bpe: BreakpointEvent =>
            vm.suspend()
            println(bpe.location.lineNumber)
            displayVaribles(bpe)

          case ste: StepEvent =>
            println(ste.location.lineNumber)
            displayVaribles(ste)

          case ev =>
            println(s"unhandled event: ${ev}")

        }
        vm.resume()
      }
    } catch {
      case disconnected: VMDisconnectedException =>
        println("vm disconnected")
    }
  }

}
