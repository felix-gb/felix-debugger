package felixdb

import java.util.concurrent.LinkedBlockingQueue
import upickle.default.{write, read}
import scala.concurrent.{Future, blocking}

object DebuggerServer extends cask.MainRoutes {
  import CommandMessage.commandMessageRW

  val messages = new LinkedBlockingQueue[DebugMessage]()
  val commands = new LinkedBlockingQueue[CommandMessage]()

  @cask.websocket("/debugger")
  def debugger(): cask.WebsocketResult =
    cask.WsHandler { channel =>
      cask.WsActor {
        case cask.Ws.Text("start") =>
          Future {
            blocking {
              while (true) {
                val msg = messages.take()
                channel.send(cask.Ws.Text(write(msg)))
              }
            }
          }
        case cask.Ws.Text(msg) =>
          println(msg)
          val command = read[CommandMessage](msg)
          commands.add(command)
      }
    }

  val debugThread = new Thread {
    override def run(): Unit =
      while (true) {
        val connector = Connector.findAttachingConnector()
        val vm = Connector.attach(connector)
        val bp = Breakpoint(37)
        new Events(vm, List(bp), messages, commands).start()
      }
  }

  debugThread.start()

  initialize()

}
