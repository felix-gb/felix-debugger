package felixdb

import upickle.default.{ReadWriter => RW, macroRW}

sealed abstract class DebugMessage

object DebugMessage {

  final case class Varible(
    threadName: String,
    name: String,
    `type`: String,
    value: String
  ) extends DebugMessage

  implicit val varibleRW = macroRW[Varible]
  implicit val messageRW = RW.merge[DebugMessage](varibleRW)

}

sealed abstract class CommandMessage

object CommandMessage {

  final case class Step(threadName: String) extends CommandMessage

  implicit val stepRW = macroRW[Step]
  implicit val commandMessageRW = RW.merge[CommandMessage](stepRW)

}
